<?php
/*
Plugin Name: Push Settings
Plugin URI: http://www.jold.nl
Plugin Domain: jld_pushsettings
Description: Push or update a set of predefined presets and settings. 
Version: 0.1
Author: JOLD Interactive; Jurgen Oldenburg
Author URI: http://www.jold.nl
*/



// create custom plugin settings menu
add_action('admin_menu', 'jld_exportsettings_menu');
function jld_exportsettings_menu() {
  add_menu_page('Export settings', 'Export settings', 'administrator', __FILE__, 'jld_exportsettings_page',plugins_url('/images/icon.png', __FILE__));
  add_action( 'admin_init', 'register_mysettings' );
}

 

function register_mysettings() {
  //register our settings
  register_setting( 'baw-settings-group', 'new_option_name' );
  register_setting( 'baw-settings-group', 'some_other_option' );
  register_setting( 'baw-settings-group', 'option_etc' );
}


function read_settings() {
  $allowedExts = array("gif", "jpeg", "jpg", "png", "txt");
  $temp = explode(".", $_FILES["file"]["name"]);
  $extension = end($temp);
  if ((($_FILES["file"]["type"] == "image/gif")
  || ($_FILES["file"]["type"] == "image/jpeg")
  || ($_FILES["file"]["type"] == "image/jpg")
  || ($_FILES["file"]["type"] == "image/pjpeg")
  || ($_FILES["file"]["type"] == "image/x-png")
  || ($_FILES["file"]["type"] == "text/plain")
  || ($_FILES["file"]["type"] == "image/png"))
  && ($_FILES["file"]["size"] < 20000)
  && in_array($extension, $allowedExts)) {
    if ($_FILES["file"]["error"] > 0) {
      echo "Error: " . $_FILES["file"]["error"] . "<br>";
      print_r($_FILES);
    }
    else {
/*
      echo "Upload: " . $_FILES["file"]["name"] . "<br>";
      echo "Type: " . $_FILES["file"]["type"] . "<br>";
      echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
      echo "Stored in: " . $_FILES["file"]["tmp_name"];
*/
      //      print_r($_FILES);
      $settings_imported = file_get_contents($_FILES['file']['tmp_name']);
      $settings_array = unserialize($settings_imported);

//      echo "<pre>";
//      print_r($settings_array);

      foreach ($settings_array as $setting => $value) {
        echo " setting-> ";
        echo($value["setting"]);
        echo " value-> ";
        echo($value["value"]);
        echo "<br />";
/*
        */
        update_option("JLD-" . $value["setting"], $value["value"]);
      }
//      echo "</pre>";
      
    }
  }
  else {
    echo "Invalid file";
  }
}


function loop_settings(){

  $settings_array = array(
    "thumbnail_size_w" => "483",
    "setting_01" => "aaa",
    "setting_02" => "bbb",
    "setting_03" => "ccc",
  );

  foreach ($settings_array as $setting => $value) {
    update_option($setting, $value);
  }
}

if(isset($_POST["update_settings"])){
  jld_exportsettings_download();
}


if(isset($_POST["download_settings"])){
//  jld_exportsettings_download();
}


if(isset($_POST["upload_settings"])){
  read_settings();
}


function jld_exportsettings_download() {

  $submitresults = $_POST["checkbox"];
  $str = "";
  $output_array = array();

  foreach ($submitresults as $setting) {
    if ( !is_array(get_option($setting)) ) {
      $settingVal = htmlentities(get_option($setting));
    }
    else {
      $settingVal = serialize(get_option($setting));
    }
    array_push($output_array, array('setting' => $setting, "value" => $settingVal) );
    $str .=  "update_option('" . $setting . "', '" . $settingVal . "');\n";
  }
  $array_length = serialize($output_array);
  $filename = "settings-" . strtolower(str_replace(" ","_",get_bloginfo('name'))) . "-" . date("YmdHis") . ".txt";
  header('Content-Disposition: attachment; filename="'.$filename.'"');
  header('Content-Type: text/plain'); # Don't use application/force-download - it's not a real MIME type, and the Content-Disposition header is sufficient
  header('Content-Length: ' . strlen($array_length));
  header('Connection: close');
  echo($array_length);
  exit;
}


function jld_exportsettings_page() {
?>
  <div class="wrap">
    <h2>Export settings</h2>
    <p>Select some settings from the table below to export them into a settings file. You can then import them into another WordPress Site.</p>

    <div class="updated settings-error">
      <p>
        <code style="background-color: transparent; padding: 0px 0px 0px 0px;">
<?php
  if(isset($_POST["update_settings"])){

    $submitresults = $_POST["checkbox"];
    $output_code = "";
    $output_array = array();
    
    foreach ($submitresults as $setting) {
      if ( !is_array(get_option($setting)) ) {
        $settingVal = htmlentities(get_option($setting));
      }
      else {
        $settingVal = serialize(get_option($setting));
      }
      array_push($output_array, array('setting' => $setting, "value" => $settingVal) );
      $str .=  "update_option('" . $setting . "', '" . $settingVal . "');\n";
    }

    echo "<pre>";
    print_r($output_array);
    echo "</pre>";
  }
?>
        </code>
      </p>
    </div>



    <form method="post" enctype="multipart/form-data">
      <table class="form-table">
        <tr>
          <th style="width: 20px;"><label for="file">Filename:</label></th>
          <td style="width: 20px;"><input type="file" name="file" id="file"></td>
        </tr>
      </table>
      <p class="submit">
        <input type="submit" name="upload_settings" id="upload_settings" class="button-primary" value="<?php _e('Upload settings file') ?>" />
      </p>
    </form>




    <form method="post" action="">
      <table class="form-table">
        <tr>
          <th style="width: 20px;"> </th>
          <th style="width: 20px;">ID</th>
          <th style="width: 100px;">Name</th>
          <th style="width: auto;">Value</th>
        </tr>

<?php
  global $wpdb;
  $options = $wpdb->get_results( "SELECT * FROM $wpdb->options ORDER BY option_name" );
  // echo "<pre>"; print_r($options); echo "</pre>";

    foreach ( (array) $options as $option ) {
    $startchar = substr($option->option_name, 0, 1);

    // filter out all _transient and _site settings
    if($startchar != "_"){
      
?>
        <tr>
          <td style="width: 20px;">
            <input type="checkbox" name="checkbox[]" id="checkbox[]" value="<?php echo $option->option_name; ?>" />
            <input type="hidden" name="<?php echo $option->option_name; ?>" value="<?php if ( !is_serialized( $option->option_value ) ) { echo $option->option_value; } else {print_r( $option->option_value ); } ?>" />
          </td>
          <td style="width: 20px;"><?php echo $option->option_id; ?></td>
          <td style="width: 200px;"><?php echo $option->option_name; ?></td>
          <td style="width: auto"><?php echo $option->option_value; ?></td>
        </tr>
<?php
    }
  }
?>
      </table>
      <p class="submit">
        <input type="submit" name="update_settings" id="update_settings" class="button-primary" value="<?php _e('Save selected settings to file') ?>" />
      </p>
    </form>
  </div>

<?php
/*




foreach ( (array) $options as $option ) :
  $disabled = false;
  if ( $option->option_name == '' )
    continue;
  if ( is_serialized( $option->option_value ) ) {
    if ( is_serialized_string( $option->option_value ) ) {
      // this is a serialized string, so we should display it
      $value = maybe_unserialize( $option->option_value );
      $options_to_update[] = $option->option_name;
      $class = 'all-options';
    } else {
//      $value = 'SERIALIZED DATA';
      $value = print_r( $option->option_value , true);
      $disabled = true;
      $class = 'all-options disabled';
    }
  } else {
    $value = $option->option_value;
    $options_to_update[] = $option->option_name;
    $class = 'all-options';
  }
  $name = esc_attr( $option->option_name );
  echo "
<tr>
  <th scope='row'><label for='$name'>" . esc_html( $option->option_name ) . "</label></th>
<td>";
  if ( strpos( $value, "\n" ) !== false )
    echo "<textarea class='$class' name='$name' id='$name' cols='30' rows='5'>" . esc_textarea( $value ) . "</textarea>";
  else
    echo "<input class='regular-text $class' type='text' name='$name' id='$name' value='" . esc_attr( $value ) . "'" . disabled( $disabled, true, false ) . " />";
  echo "</td>
</tr>";
endforeach;
*/
?>

<?php }














